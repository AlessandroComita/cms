<?php
    
    include 'includes/header.php';
    include 'includes/functions.php';

?>

<body>

    <!-- Navigation -->
    <?php
        include 'includes/navigation.php';
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

            <?php
                $posts = search_posts();
                show_posts($posts);
                
            ?>


                

                

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php

                include 'includes/sidebar.php';
            ?>

            

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <?php
            include 'includes/footer.php';
        ?>

