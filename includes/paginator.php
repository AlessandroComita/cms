<ul class="pager">
    <?php
        $post_pages = set_page_number($posts_per_page);

        for ($i = 1; $i <= $post_pages; $i++)
        {

            $current_page = get_current_page();
            
            if ($i == $current_page)
            {
                $page_button = 'active_link';
            }
            else
            {
                $page_button = 'unactive';
            }
            
            echo 
            "
            <li>
                <a 
                    class = '{$page_button}'
                    href='index.php?page={$i}'>{$i}</a>
            </li>
            ";
        }
    ?>

</ul>