<?php 
    session_start();
    include 'functions.php';
?>

<?php

    if (isset($_POST['login_submit']))
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        // ripulire i dati scritti nel campo da codice pericoloso - ad esempio, per il database
        $username = mysqli_real_escape_string($connection, $username);
        $password = mysqli_real_escape_string($connection, $password);
    }


    $query = 
    "
        SELECT * FROM users
        WHERE username = '{$username}'
    ";

    $select_user_query = mysqli_query($connection, $query);

    $context = 'select user (login)';
    if (control_query($select_user_query, $context))
    {
        while ($row = mysqli_fetch_array($select_user_query))
        {
            echo " with ID ";
            echo $db_id = $row['user_id'];

            $db_id = $row['user_id'];
            $db_username = $row['username'];
            $db_user_password = $row['user_password'];
            $db_user_firstname = $row['user_firstname'];
            $db_user_lastname = $row['user_lastname'];
            $db_user_image = $row['user_image'];
            $db_user_role = $row['user_role'];
            
        }

        // $password = crypt($password, $db_user_password);

        if (password_verify($password, $db_user_password))
        {
            $_SESSION['user_id'] = $db_id;
            $_SESSION['username'] = $db_username;
            $_SESSION['user_firstname'] = $db_user_firstname;
            $_SESSION['user_lastname'] = $db_user_lastname;
            $_SESSION['user_image'] = $db_user_image;
            $_SESSION['user_role'] = $db_user_role;
            header ("Location: ../admin/");           
        }
        else
        {
            echo 
            "
                <script>
                    console.warn('credentials not matching');
                </script>
            ";
            header ("Location: ../index.php?auth=none");
        }
        
    }

?>