<?php 
    session_start();
    include 'functions.php';
?>

<?php

    global $connection;

    if (isset($_GET['logout']))
    {
        unset($_SESSION['username']);
        unset($_SESSION['user_role']);
        unset($_SESSION['user_firstname']);
        unset($_SESSION['user_lastname']);
        header ("Location: ../index.php?none");
    }

?>