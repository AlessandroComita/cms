<?php
    $post_category_id = $post['post_category_id'];
    $category = get_category_by_id($post_category_id);
    if (!$category)
    {
        $category['cat_title'] = 'Uncategorized';
        $post_category_id = 47;
    }
?>

<h1 class='page-header'>
    <?php  
        echo $post['post_title'];  
    ?>
</h1>

<!-- First Blog Post -->

<p class='lead'>
    by <a 
            href="author.php?author=<?php echo $post['post_author']; ?>&p_id=<?php echo $post['post_id']; ?>"
            style='text-decoration: none; color: darkred'    
        >
        <?php echo $post['post_author']; ?>
       </a>
</p>

<p class='lead'>
    in 
    <a href="<?php echo "category.php?category=$post_category_id"; ?>">
        <?php echo $category['cat_title']; ?>
    </a>
</p>


<p><span class='glyphicon glyphicon-time'></span> 
    <?php
        echo $post['post_date'];
    ?>
</p>

<hr>
<img class='img-responsive' 
    src="<?php echo "images/{$post['post_image']}"; ?> " alt=''>
<hr>

<p>
    <?php
        echo $post['post_content'];
    ?>
</p>


<hr>