<div class="col-md-4">




    <!-- Login -->
    <div class="well">
        <h4>Login</h4>

        <form action="includes/login.php" method="POST">
            <div class="form-group">
                
                <input name="username" type="text" class="form-control"
                    placeholder="Enter username">
            </div>

            <div class="input-group">
                
                <input 
                    name="password" 
                    type="password"
                    class="form-control"
                    placeholder="Enter password"
                />
                <span class="input-group-btn">
                    <button 
                        name="login_submit"
                        type="submit"
                        class="btn btn-primary">Submit
                    </button>
                </span>
            </div>

            

        </form>

        <!-- /.input-group -->
    </div>








    <!-- Blog Search Well -->
    <div class="well">
        <h4>Blog Search</h4>

        <form action="search.php" method="POST">
        <div class="input-group">
            <input name="search" type="text" class="form-control">
            <span class="input-group-btn">
                <button name="search_submit" class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
            </button>
            </span>
        </div>

        </form>

        <!-- /.input-group -->
    </div>






                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                        <?php
                            show_categories_well();
                        ?>
                            </ul>
                        </div>

                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <!-- right column, empty for now -->
                            </ul>
                        </div>


                    </div>
                </div>

                <?php

                    include "includes/widget.php";
                ?>

            </div>