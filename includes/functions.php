<?php

// *** VARIABLE / COSTANT DECLARATION

// la variabile «connection» globale esce fuori da qui
$connection = establish_connection('localhost', 'root', '', 'cms');



// *** UTILITIES

function establish_connection($host, $user, $pass, $database)
{
    $port = '3307';

    $db['db_host'] = $host . ':' . $port;
    $db['db_user'] = $user;
    $db['db_pass'] = $pass;
    $db['db_name'] = $database;
    

    // tramite questo stratagemma, di usare un array per conservare i dati della connessione
    // si può usare define per definire una costante runtime
    // che peraltro la funzione strtoupper si preoccupa di scrivere in maiuscolo
    // la scelta di assegnare i dati della connessione a delle costanti risponde a ragioni di sicurezza
    foreach($db as $key => $value)
    {
        define(strtoupper($key), $value);
    }
 
    $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    
    if($conn)
    {
        echo 
        "
            <script>
                console.log('%c connection to database ' + '[{$database}]' + ' OK', 'color: #green;');    
            </script>
        ";
        return $conn;
    }
    else
    {
        echo 
        "
            <script>
                console.log('%c connection to database ' + '[{$database}]' + ' FAILED', 'color: #D04608;');    
            </script>
        ";
    }
}


function alert_to_user()
{

    if (isset($_SESSION['message']))
    {
        echo 
        "
        <div 
            class='text-right'
            style='padding-bottom: 15px; margin-top: 35px;'>
            <span 
                
                style=
                '
                        
                        padding: 15px 10px 15px 10px;
                        background-color: #0066b2;
                        color: white;
                        border-radius: 5px;
                        
                '>
                {$_SESSION['message']}       
            </span>
        </div>
        ";

        unset($_SESSION['message']);
    }
}


function control_query($result, $context)
{
    global $connection;

    if ($result)
    {
        echo
         "
            <script>
                console.info('%c Query ' + '«{$context}»' + ' OK', 'color: lime;');
                
            </script>
         ";
        return true;
    }
    else
    {
        die('Query failed > ' . mysqli_error($connection));
        return false;
    }
}


function control_and_return_query($result, $context)
{
    global $connection;

    if ($result)
    {
        echo
         "
            <script>
                console.info('%c Query ' + '{$context}' + ' OK', 'color: lime;');
                
            </script>
         ";
        return $result;
    }
    else
    {
        die ('Query failed > ' . mysqli_error($connection));
    }
}


// *** POSTS 

function create_post()
{
    global $connection;

    if(isset($_POST['create_post']))
    {
        $post_title = $_POST['title'];
        $post_author = $_POST['author'];
        $post_category_id = $_POST['post_category_id'];
        $post_status = $_POST['post_status'];
        
        $post_image = $_FILES['image']['name'];
        $post_image_temp = $_FILES['image']['tmp_name'];
        
        $post_tags = $_POST['tags'];
        $post_content = $_POST['content'];

        
        
        // sposta l'immagine inserita dall'utente dalla cartella temporanea alla cartella che effettivamente contiene le immagini del progetto; la cartella temporanea è quella in cui l'immagine viene temporaneamente collocata dopo l'upload e prima del submit da parte dell'utente.
        move_uploaded_file($post_image_temp, "../images/$post_image");

        $query = 
        "
        INSERT INTO posts 
            (
            post_category_id, post_title, 
            post_author, post_date, post_image, post_content, 
            post_tags, post_status
            )
    
        VALUES
            (
            '{$post_category_id}', '{$post_title}', 
            '{$post_author}', now(), '{$post_image}', '{$post_content}', 
            '{$post_tags}', '{$post_status}'
            ) 
        ";
        
        $create_post_query = mysqli_query($connection, $query);
                
        $_SESSION['current_query'] = $create_post_query;
        $context = 'create post';


        if (control_query($create_post_query, $context))
        {
            $_SESSION['fm_message'] = 'Post Created';
            // $last_post = get_last_post();
            $last_post = mysqli_insert_id($connection);
            $_SESSION['fm_element_id'] = $last_post;  
            // unset($_SESSION['fm_element_id']);
        }

        



        header('Location: posts.php');

    }
}


function select_all_posts()
{
    global $connection;

    $query = 
    "
    SELECT * FROM posts
    ORDER BY post_date DESC;
    ";

    $context = 'select all post ordered by date desc';
    $select_all_posts_query = mysqli_query($connection, $query);

    if (control_query($select_all_posts_query, $context))
    {
        return $select_all_posts_query;
    }
}


function select_all_published_posts($from, $posts_per_page)
{
    global $connection;

    $query = 
    "
    SELECT * FROM posts
    WHERE post_status = 'published'
    ORDER BY post_date DESC
    LIMIT $from, $posts_per_page
    ";

    $context = 'select all post ordered by date desc, paginated';
    $select_all_posts_query = mysqli_query($connection, $query);

    if (control_query($select_all_posts_query, $context))
    {
        return $select_all_posts_query;
    }
}


function select_post($post_id)
{
    global $connection;

    $query = "SELECT * FROM posts WHERE post_id = $post_id ";
    
    $select_post_query = mysqli_query($connection, $query);
    $context = 'select post';
    if (control_query($select_post_query, $context))
    {
        $post = mysqli_fetch_assoc($select_post_query);
        return $post;
    }
}

function select_posts_by_author($author)
{
    global $connection;

    $query = "SELECT * FROM posts WHERE post_author = '$author' ";
    
    $select_posts_by_author = mysqli_query($connection, $query);
    $context = 'select posts by author';
    if (control_query($select_posts_by_author, $context))
    {
        return $select_posts_by_author;
    }
}


function search_posts()
{
    global $connection;

    $search = $_POST['search'];
    $query = "SELECT * FROM posts WHERE post_tags LIKE '%$search%' ";
    $search_query = mysqli_query($connection, $query);
    return $search_query;
}


function search_posts_by_id($category_id)
{
    global $connection;

    $query = "SELECT * FROM posts WHERE post_category_id = $category_id ";
    $search_query = mysqli_query($connection, $query);
    $context = 'select posts by id';
    if (control_query($search_query, $context))
    {
        return $search_query; 
    }
    
}


function show_post($post)
{
    increment_post_views($post);

    // il parametro formale $post è "spento" come se non venisse utilizzato all'interno del codice; in realtà, il parser di Visual Studio Code non può accorgersi che esso viene utilizzato dentro la pagina includes/post.php.
    include "includes/post.php";
}


function show_posts($posts)
{

    if (!$posts->num_rows)
    {
        echo "<h1>No Results</h1>";
    }
    else
    {
        while($row = mysqli_fetch_assoc($posts))
        {
            $post_id = $row['post_id'];
            $post_title = $row['post_title'];
            $post_author = $row['post_author'];
            $post_date = $row['post_date'];
            $post_image = $row['post_image'];

            $post_category_id = $row['post_category_id'];
            $category = get_category_by_id($post_category_id);

            if (!$category)
            {
                $category['cat_title'] = 'Uncategorized';
            }
            
            $content_length = strlen($row['post_content']);
            if ($content_length >= 100)
            {
                $truncate = "[...]";
            }
            else
            {
                $truncate = "";
            }
            $post_content = substr($row['post_content'], 0, 100);
    
            // inserisci contenuto html del post, cioè il layout con iniettato il contenuto tramite php
    
            echo 
            "
            
                        <h1 class='page-header'>
                            <a 
                                style='text-decoration: none; color: black;'
                            
                                href='post.php?p_id={$post_id}'>
                            {$post_title}
                                


                            </a>
                        </h1>
    
                        <!-- First Blog Post -->

                        <p class='lead'>
                            by <a 
                                    style='text-decoration: none; color: darkred;'
                                    href='author.php?author={$post_author}&p_id={$post_id}'>
                                {$post_author}
                                </a>
                        </p>

                        <p class='lead'>
                        in <a href='category.php?category=$post_category_id'>
                                {$category['cat_title']}
                           </a>
                        </p>
                        


                        <p><span class='glyphicon glyphicon-time'></span> 
                    
                        
                                    {$post_date}
                            
                    
                        </p>
    
                        <hr>
                        <a href='post.php?p_id={$post_id}'>
                            <img class='img-responsive' 
                                src='images/{$post_image}' 
                            
                                alt=''>
                        </a>
                        <hr>
                        <p>
                            
                        
                                    {$post_content}{$truncate}
                                
    
                        </p>
                        
                        <a class='btn btn-primary' href='post.php?p_id={$post_id}'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>
    
                        <hr>
            ";
        // close the while loop for post structure/content
        };
    }                 
};


function show_posts_table($posts)
{

    global $connection;

    $categories = select_all_categories();
    $category_name = 'uncategorized';

    if (!$posts->num_rows)
    {
        echo "<h1>No Results</h1>";
    }
    else
    {
        while($row = mysqli_fetch_assoc($posts))
        {
            $post_id = $row['post_id'];
            $post_author = $row['post_author'];
            $post_title = $row['post_title'];
            $post_category_id = $row['post_category_id'];
            foreach ($categories as $category)
            {
                if ($post_category_id == $category['cat_id'])
                {
                    $category_name = $category['cat_title'];
                }   
            }
            $post_content = $row['post_content'];
            $truncated_content = strip_tags(trim(substr($post_content, 0, 30)));

            if (strlen($truncated_content) >= 30)
            {
                $truncated_content = $truncated_content . ' [...]';
            }

            if ($truncated_content == null)
            {
                $truncated_content = 
                "<i style='color: darkred'>
                it cannot be displayed in this view</i>
                ";
            }

            $post_image = $row['post_image'];
            $post_tags = $row['post_tags'];
            $post_status = $row['post_status'];
            $post_views = $row['post_views'];

            if ($post_status == 'published')
            {
                $display_publish = 'none';
            }
            else
            {
                $display_publish = 'block';
            }

			if ($post_views <= 0)
			{
				$display_reset = 'none';
			}
			else
			{
				$display_reset = 'block';
			}


            $post_date = date_create($row['post_date']);
            $date_european_format = date_format($post_date, 'd/m/y H:i:s');

            $post_views = $row['post_views'];


            // count number of comments for the specific post
            $count_comments = count_comments_number($post_id);

            // get the id of the comment for the specific post
            $comment_id = get_comment_id($post_id);



            echo
            "
            <tr>
                <td>
                    <input 
                        name='checkBoxArray[]'
                        value='{$post_id}'
                        class='checkBoxes' 
                        type='checkbox' 
                    />
                </td>
                <td>{$post_id}</td>
                <td>{$post_author}</td>
                <td>{$post_title}</td>
                <td>{$category_name}</td>
                <td>{$truncated_content}</td>
                <td>
                    <img
                        style='max-width: 5vw;' 
                        src='../images/{$post_image}'>
                </td>
                <td>{$post_tags}</td>
                <td>{$post_status}</td>


                <td>
                    <a href='comments_post.php?pc_id=$post_id'>
                        {$count_comments}
                    </a>
                </td>



                <td>{$date_european_format}</td>
                <td>{$post_views}</td>
                <td>
                    <a href='../post.php?p_id={$post_id}'>View</a>

                    <br>
                    <a href='posts.php?source=edit_post&p_id={$post_id}'>Edit</a>
                    <br>
                    <a 
                        style='display: {$display_publish}'
                        href='posts.php?post_publish={$post_id}'>
                        Publish
                    </a>

                    <a 
                        style='display: {$display_reset}'
                        href='posts.php?post_reset_views={$post_id}'>
                        <nobr>Reset Views</nobr>
                    </a>

                    <a 
                        onClick=\"javascript: return confirm('Are you sure you want to delete this post?'); \" 
                        href='posts.php?post_delete={$post_id}'
                    >
                        Delete
                    </a>
                </td>
            </tr>
            ";
        }
    }
};



function count_comments_number($post_id)
{
    global $connection;

    $query = 
    "
        SELECT * FROM comments
        WHERE comment_post_id = $post_id;
    ";
    $send_comment_query = mysqli_query($connection, $query);
    $count_comments = mysqli_num_rows($send_comment_query);

    return $count_comments;
}


function get_comment_id($post_id)
{
    global $connection;

    $query6 = 
    "
        SELECT * FROM comments
        WHERE comment_post_id = $post_id;
    ";

    $send_comment_id_query = mysqli_query($connection, $query6);

    if ($send_comment_id_query->num_rows > 0)
    {
        $row = mysqli_fetch_array($send_comment_id_query);
        $comment_id = $row['comment_id'];    
        return $comment_id;
    }
}


function search_post_by_id($post_id)
{
    global $connection;

    $query = "SELECT * FROM posts WHERE post_id = $post_id ";
    $search_query = mysqli_query($connection, $query);
    $context = 'search post by id';
    if (control_query($search_query, $context))
    {
        $post = mysqli_fetch_assoc($search_query);
        return $post;
    }
}


function get_last_post()
{
    global $connection;

    $query = 
        "
            SELECT * FROM posts 
            ORDER BY post_date DESC
            LIMIT 1
        ";
    $last_post_query = mysqli_query($connection, $query);
    $context = 'get last post';
    if (control_query($last_post_query, $context))
    {
        $post = mysqli_fetch_assoc($last_post_query);
        return $post;
    }
}


function delete_post()
{
    global $connection;

    if (isset($_GET['post_delete']))
    {
        $post_id_delete = $_GET['post_delete'];
        $query = "DELETE from posts WHERE post_id = {$post_id_delete} ";
        $delete_post_query = mysqli_query($connection, $query); 
        $context = 'delete post';


        if (control_query($delete_post_query, $context))
        {
            $_SESSION['fm_message'] = 'Post Deleted';
            unset($_SESSION['fm_element_id']);
        }       
        header('Location: posts.php');
    }
}


function delete_post_by_id($id)
{
    global $connection;

    $query = "DELETE FROM posts WHERE post_id = {$id} ";
    $delete_post_query = mysqli_query($connection, $query); 
    $context = 'delete post by id';
    if (control_query($delete_post_query, $context))
    {
        $_SESSION['fm_message'] = 'Post(s) Deleted';
        unset($_SESSION['fm_element_id']);
    }
    header('Location: posts.php');
}


function get_edit_post()
{
    global $connection;

    if (isset($_GET['p_id']))
    {
        $post_id_edit = $_GET['p_id'];

        $query = "SELECT * FROM posts WHERE post_id = '$post_id_edit' ";
        
        $select_edit_query = mysqli_query($connection, $query);
        $row = mysqli_fetch_assoc($select_edit_query);

        return $row;
    }
}


function update_post($id_post_edit, $image_path)
{
    global $connection; 

    if (isset($_POST['submit_post_edit']))
    {
        $post_title = $_POST['title'];
        $post_author = $_POST['author'];
        $post_category_id = $_POST['post_category_id'];
        $post_status = $_POST['post_status'];
        
        // $post_image = $image_path;
        $post_image = $_FILES['image']['name'];
        $post_image_temp = $_FILES['image']['tmp_name'];
        
        $post_tags = $_POST['tags'];
        $post_content = $_POST['content'];

        $post_comment_count = 4;


        // sposta l'immagine inserita dall'utente dalla cartella temporanea alla cartella che effettivamente contiene le immagini del progetto; la cartella temporanea è quella in cui l'immagine viene temporaneamente collocata dopo l'upload e prima del submit da parte dell'utente.
        move_uploaded_file($post_image_temp, "../images/$post_image");

            if(empty($post_image))
            {
                $post_image = $image_path;
            }

        $query = 
        "
        UPDATE posts 
        SET
            post_category_id = {$post_category_id},
            post_title = '{$post_title}',
            post_author = '{$post_author}',
            post_date = now(),
            post_image = '{$post_image}',
            post_content = '{$post_content}',
            post_tags = '{$post_tags}',
            post_comment_count = {$post_comment_count},
            post_status = '{$post_status}'     
        WHERE post_id = {$id_post_edit};
        ";

        $update_post_query = mysqli_query($connection, $query);
        
        $context = 'update post';

        if (control_query($update_post_query, $context))
        {
            $_SESSION['fm_message'] = 'Post Updated';
            $_SESSION['fm_element_id'] = $id_post_edit;
        }

        header('Location: posts.php');
    }
}


function publish_post()
{
    global $connection;

    if (isset($_GET['post_publish']))
    {
        $post_id_publish = $_GET['post_publish'];

        $query = 
        "
            UPDATE posts
            SET post_status = 'published'
            WHERE post_id = $post_id_publish;
        ";
       
        $publish_post_query = mysqli_query($connection, $query); 
        $context = 'publish post';


        if (control_query($publish_post_query, $context))
        {
            $_SESSION['fm_message'] = 'Post Published';
            $_SESSION['fm_element_id'] = $post_id_publish;
        }
        
        header('Location: posts.php');
    }
}


function update_post_status($status, $id)
{
    global $connection; 

    $query = 
    "
        UPDATE posts
        SET post_status = '{$status}'
        WHERE post_id = $id;
    ";
   
    $update_post_status_query = mysqli_query($connection, $query); 
    $context = 'update post status';

    if (control_query($update_post_status_query, $context))
    {
        $_SESSION['fm_message'] = 'Status Updated';
        unset($_SESSION['fm_element_id']);
        header('Location: posts.php');
    }
}


function reset_post_views($id)
{
    global $connection;

    $query = 
    "
        UPDATE posts
        SET post_views = 0
        WHERE post_id = $id;
    ";
    
    $views_reset_query = mysqli_query($connection, $query); 
    $context = 'reset post views';

    if (control_query($views_reset_query, $context))
    {
        $_SESSION['fm_message'] = 'Views Resetted';
        unset($_SESSION['fm_element_id']);
        header('Location: posts.php');
    }
}


function clone_post($id)
{
    global $connection;

    $query =
    "
        SELECT * FROM posts
        WHERE post_id = {$id};
    ";

    $query_copy_post = mysqli_query($connection, $query);

    $context = 'copy post content';
    if (control_query($query_copy_post, $context))
    {
        while ($row = mysqli_fetch_array($query_copy_post))
        {
            $post_category_id = $row['post_category_id'];
            $post_title = $row['post_title'];
            $post_author = $row['post_author'];
            $post_category_id = $row['post_category_id'];
            $post_status = $row['post_status'];
            $post_image = $row['post_image'];

            $post_content = $row['post_content'];            
            $post_tags = $row['post_tags'];
            $post_status = $row['post_status'];

    
            $post_comment_count = $row['post_comment_count'];


            $query = 
            "
            INSERT INTO posts 
                (
                post_category_id, 
                post_title, 
                post_author, post_date, post_image, 
                post_content, 
                post_tags, 
                post_comment_count, 
                post_status
                )
        
            VALUES
                (
                '{$post_category_id}', 
                '{$post_title}', 
                '{$post_author}', now(), 
                '{$post_image}', 
                '{$post_content}', 
                '{$post_tags}', 
                $post_comment_count, 
                '{$post_status}'
                ) 
            ";

                $clone_post_query = mysqli_query($connection, $query);
                
                $context = 'clone post';

                if (control_query($clone_post_query, $context))
                {
                    $_SESSION['fm_message'] = 'Post Cloned';
                    $_SESSION['fm_element_id'] = $id;
                }

                header('Location: posts.php');


        }
    }
}


function count_posts()
{
    global $connection;

    $query = "SELECT * FROM posts";
    $select_all_post = mysqli_query($connection, $query);
    $post_count = mysqli_num_rows($select_all_post);

    return $post_count;
}


function count_posts_by_status($status)
{
    global $connection;

    $query = "SELECT * FROM posts where post_status = '{$status}' ";
    $select_posts_by_status = mysqli_query($connection, $query);
    $post_count = mysqli_num_rows($select_posts_by_status);

    return $post_count;
}


function increment_post_views($post)
{
    global $connection;
    
    $id = $post['post_id']; 

    $query =
    "
        UPDATE posts
        SET post_views = post_views + 1
        WHERE post_id = $id;
    ";

    $increment_view_query = mysqli_query($connection, $query);

    $context = 'views number incremented';
    control_query($increment_view_query, $context);
}


function set_page_number($posts_per_page)
{
    $posts_number = count_posts();

    $post_pages = ceil($posts_number / $posts_per_page);

    return $post_pages;
}


function set_starting_post($posts_per_page)
{
    if (isset($_GET['page']))
    {
        $page = $_GET['page'];
        $from = ($page - 1) * $posts_per_page;
    }
    else
    {
        $from = 0;
    }

    return $from;
}

function get_current_page()
{
    if (isset($_GET['page']))
    {
        $current_page = $_GET['page'];
    }
    else
    {
        $current_page = 1;
    }
    return $current_page;
}





// *** CATEGORIES

function select_all_categories()
{
    global $connection;

    $query = "SELECT * FROM categories ";
    $select_all_categories_query = mysqli_query($connection, $query);

    $context = 'select all categories';
    if (control_query($select_all_categories_query, $context))
    {
        return $select_all_categories_query;
    }
}

function show_categories()
{
    $categories = select_all_categories();

    echo
    "
    <div class='dropdown'>
        <a 
            id='dropdownMenuLink' 
            class='btn btn-secondary dropdown-toggle' 
            style=
            '
                text-decoration: none; 
                color: white;
                padding: 15px;
                margin: 0px;
                border: 0px;
                background: transparent;
            '
            href='#' 
            role='button' 
            
            data-toggle='dropdown' 
            aria-haspopup='true' 
            aria-expanded='false'>
                Categories
        </a>

    <div class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
    "; 

    while($row = mysqli_fetch_assoc($categories))
    {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
        echo 
        "
            <a 
                class='dropdown-item'
                style=
                '
                text-decoration: none; 
                color: darkred;
                padding-left: 5px;
                ' 
                href='category.php?category=$cat_id'>
                {$cat_title}
            </a>
            <br>
        ";
    } 
    
    echo
    "
    </div>
    </div>
    ";
}

function show_categories_well()
{
    $categories = select_all_categories();

    while($row = mysqli_fetch_assoc($categories))
    {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
        echo 
        "
        <li>
            <a 
                class='dropdown-item'
                style=
                '
                text-decoration: none; 
                color: darkred;
                padding-left: 5px;
                ' 
                href='category.php?category=$cat_id'>
                {$cat_title}
            </a>
            
        </li>
        ";
    } 
}


function show_categories_table($categories)
{
    while($row = mysqli_fetch_assoc($categories))
    {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
        
        echo 
        "                                    
            <tr>
                <td>{$cat_id}</td>
                <td>{$cat_title}</td>
                <td>
                    <a href='categories.php?delete={$cat_id}'>Delete</a> | 
                    <a href='categories.php?update={$cat_id}'>Update</a>
                </td>
            </tr>
        ";
    };
    
    
    if (isset($_GET['delete']))        
    {
        delete_category();
    }
    
    if (isset($_GET['update']))
    {
        update_category($_GET['update']);
    }

}

function delete_category()
{
    global $connection;

    if(isset($_GET['delete']))
    {
        $id = $_GET['delete'];
        $query = "DELETE FROM categories WHERE cat_id = {$id} ";
        $delete_query = mysqli_query($connection, $query);

        $context = 'delete category';
        if (control_query($delete_query, $context))
        {
            $_SESSION['fm_message'] = 'Category Deleted';
            unset($_SESSION['fm_element_id']);
        }
        
        // refresh page >>> another request for the page
        header('Location: categories.php');

    }
}

function get_category()
{
    global $connection;

    if (isset($_GET['update']))
    {
        $id = $_GET['update'];
        $query = "SELECT * FROM categories WHERE cat_id = '$id' ";
        $select_edit_query = mysqli_query($connection, $query);
        $row = mysqli_fetch_assoc($select_edit_query);

        return $row;
        
    }
}


function get_category_by_id($id)
{
    global $connection;

    $query = "SELECT * FROM categories WHERE cat_id = '$id' ";
    $select_edit_query = mysqli_query($connection, $query);
    $context = 'get category';
    if (control_query($select_edit_query, $context))
    {
        $category = mysqli_fetch_assoc($select_edit_query);
        return $category;
    }
}


function update_category($id)
{
    
    global $connection; 

    if (isset($_POST['cat_edit_submit']))
    {
        $title = $_POST['cat_title_edit'];
        echo 
        "
        <script>
            console.log('{$title}');
        </script>
        ";

        $query = "UPDATE categories ";
        $query .= "SET cat_title = '$title' ";
        $query .= "WHERE cat_id = '$id' ";
        $query_edit = mysqli_query($connection, $query);

        if (!$query_edit)
        {
            die('Query failed!' . mysqli_error($connection));
        };

        $context = 'update query';
        if (control_query($query_edit, $context))
        {
            $_SESSION['fm_message'] = 'Category Updated';
            unset($_SESSION['fm_element_id']);
        }

        header('Location: categories.php');
    }
}


function add_category()
{
    global $connection;
    if(isset($_POST['cat_add_submit']))
    {
        $cat_title = $_POST['cat_title'];
        if($cat_title == "" || empty($cat_title))
        {
            echo "this field should not be empty";
        }
        else
        {
            $query = "INSERT INTO categories(cat_title) ";
            $query .= " VALUE('$cat_title') ";

            $create_category_query = mysqli_query($connection, $query);

            if(!$create_category_query)
            {
                die ("adding query failed" . mysqli_error($connection));
            }


            $context = 'add query';
            if (control_query($create_category_query, $context))
            {
                $_SESSION['fm_message'] = 'New Category Added';
                unset($_SESSION['fm_element_id']);
            }

            header('Location: categories.php');

        }
    }
}


function count_categories()
{
    global $connection;

    $query = "SELECT * FROM categories";
    $select_all_categories = mysqli_query($connection, $query);
    $category_count = mysqli_num_rows($select_all_categories);

    return $category_count;   
}




// USERS

function create_user()
{
    global $connection;

    if(isset($_POST['create_user']))
    {

        $username = $_POST['username'];
        $user_email = $_POST['user_email'];
        $user_password = $_POST['user_password'];
        
        $user_password = password_hash ($user_password, PASSWORD_BCRYPT, array ('cost' => 12) );

        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_role = $_POST['user_role'];

        $user_image = $_FILES['user_image']['name'];
        $user_image_temp = $_FILES['user_image']['tmp_name'];
        move_uploaded_file($user_image_temp, "../images/$user_image");


        if (!$user_image)
        {
            $user_image = 'nopicture.jpg';
        }

        $query = 
        "
        INSERT INTO users
            (
                username,
                user_email, 
                user_password,
                user_firstname, 
                user_lastname,
                user_role, 
                user_image
            )
        VALUES
            (
                '{$username}',
                '{$user_email}', 
                '{$user_password}',
                '{$user_firstname}', 
                '{$user_lastname}',
                '{$user_role}', 
                '{$user_image}'
            ) 
        ";
        
        $create_user_query = mysqli_query($connection, $query);
    
        $context = 'create user';
        if (control_query($create_user_query, $context))
        {
            $_SESSION['fm_message'] = 'User Created';
            unset($_SESSION['fm_element_id']);
            header('Location: users.php?source=add_user');
        }
    }
}


function delete_user()
{
    global $connection;

    if (isset($_GET['user_delete']))
    {
        $user_id_delete = $_GET['user_delete'];
        $query = "DELETE from users WHERE user_id = {$user_id_delete} ";
        $delete_user_query = mysqli_query($connection, $query); 
        $context = 'delete user';

        if (control_query($delete_user_query, $context))
        {
            $_SESSION['fm_message'] = 'User Deleted';
            unset($_SESSION['fm_element_id']);
        }

        header('Location: users.php');
    }

}


function make_admin_user()
{
    global $connection;

    if (isset($_GET['user_change_to_admin']))
    {

        $id_make_admin = $_GET['user_change_to_admin'];

        $query = 
        "
        UPDATE users
        SET 
            user_role = 'admin'
        WHERE user_id = $id_make_admin;
        ";

        $make_admin_query = mysqli_query($connection, $query); 

        $_SESSION['message'] = "user with id {$id_make_admin} is now admin";
        $context = 'make admin';


        if (control_query($make_admin_query, $context))
        {
            $_SESSION['fm_message'] = 'User Status<br>Updated';
            unset($_SESSION['fm_element_id']);
        }

        
        header('Location: users.php');

    }
}


function make_subscriber_user()
{

    global $connection;

    if (isset($_GET['user_change_to_subscriber']))
    {
        $id_make_subscriber = $_GET['user_change_to_subscriber'];

        $query = 
        "
            UPDATE users
            SET 
                user_role = 'subscriber'
            WHERE user_id = $id_make_subscriber;
        ";

        $make_subscriber_query = mysqli_query($connection, $query); 

        $_SESSION['message'] = "user with id {$id_make_subscriber} is now subscriber";
        $context = 'make subscriber';
        
        
        if (control_query($make_subscriber_query, $context))
        {
            $_SESSION['fm_message'] = 'User Status<br>Updated';
            unset($_SESSION['fm_element_id']);
        }

        
        header('Location: users.php');
    }

}




function get_user($user_id)
{
    global $connection;

    $query = "SELECT * FROM users WHERE user_id = $user_id ";
    $get_query = mysqli_query($connection, $query);
    $context = 'get user by id';
    if (control_query($get_query, $context))
    {
        $user = mysqli_fetch_assoc($get_query);
        return $user;
    }
    
}


function retrieve_salt()
{
    global $connection;

    $query = "SELECT randSalt FROM users ";

    $select_randsalt_query = mysqli_query($connection, $query);

    if (!$select_randsalt_query)
    {
        die('query failed' . mysqli_error($connection));
    }

    $row = mysqli_fetch_array($select_randsalt_query);
    
    $salt = $row['randSalt'];

    return $salt;
}


function update_user($user_id, $current_image)
{
    global $connection;

    if (isset($_POST['update_user']))
    {
        $username = $_POST['username'];
        $user_email = $_POST['user_email'];
        $user_password = $_POST['user_password'];

        if (!empty($user_password))
        {
            $query_password =
            "
                SELECT user_password FROM users
                WHERE user_id = $user_id
            ";

            $get_user_query = mysqli_query($connection, $query_password);
            $context = 'get user, edit page';
            control_query($get_user_query, $context);

            $row = mysqli_fetch_array($get_user_query);

            $db_user_password = $row['user_password'];
        }

        if ($db_user_password != $user_password)
        {
            $hashed_password = password_hash ($user_password, PASSWORD_BCRYPT, array ('cost' => 12) );
        }

        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_role = $_POST['user_role'];

        $user_image = $_FILES['user_image']['name'];
        $user_image_temp = $_FILES['user_image']['tmp_name'];
        move_uploaded_file($user_image_temp, "../images/$user_image");

        if (empty($user_image))
        {
            $user_image = $current_image;
        }

        // $salt = retrieve_salt();

        // $hashed_password = crypt($user_password, $salt);

        $query =
        "
            UPDATE users
            SET
                username = '{$username}',
                user_email = '{$user_email}',
                user_password = '{$hashed_password}',
                user_firstname = '{$user_firstname}',
                user_lastname = '{$user_lastname}',
                user_role = '{$user_role}',
                user_image = '{$user_image}'
            WHERE user_id = '{$user_id}'
        ";

        $query_update_user = mysqli_query($connection, $query);

        $context = 'update user';

        if (control_query($query_update_user, $context))
        {
            $_SESSION['fm_message'] = 'User Updated';
            unset($_SESSION['fm_element_id']);
        }

        header("Location: users.php");

    }
}


function select_all_users()
{
    global $connection;

    $query = "SELECT * FROM users";
    $select_all_users_query = mysqli_query($connection, $query);
    $context = 'select all users';
    if (control_query($select_all_users_query, $context))
    {
        return $select_all_users_query;
    }
}


function show_users_table($users)
{
    if (!$users->num_rows)
    {
        echo "<h1>No Results</h1>";
    }
    else
    {
        while($row = mysqli_fetch_assoc($users))
        {
            $user_id = $row['user_id'];

            $username = $row['username'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $user_email = $row['user_email'];
            $user_image = $row['user_image'];
            if($user_image)
            {
                $picture = $user_image;
            }
            else
            {
                $picture = 'nopicture.jpg';
            }

            $user_role = $row['user_role'];

            echo
            "
            <tr>
                <td>{$user_id}</td>
                <td>{$username}</td>
                <td>{$user_firstname}</td>
                <td>{$user_lastname}</td>
                <td>{$user_email}</td>
                
                <td
                    style='vertical-align: middle;
                    text-align: center;
                    ';
                >
                    <img
                        style='max-width: 5vw;' 
                        src='../images/{$picture}'>
                </td>


                <td>{$user_role}</td>
                <td>
            ";
                if ($user_role == 'subscriber')
                {
                    echo
                    "
                    <a href='users.php?user_change_to_admin={$user_id}'>Make Admin</a>
                        <br> 
                    ";
                }
                elseif ($user_role == 'admin')
                {
                    echo
                    "
                        <a href='users.php?user_change_to_subscriber={$user_id}'>Make Subscriber</a>
                        <br> 
                    ";

                }
            echo
            "
                    <a href='users.php?user_delete={$user_id}'>Delete</a> 
                    <br> 
                    <a 
                        href='users.php?source=edit_user&u_id={$user_id}'>
                        Edit
                    </a> 
                </td>
            </tr>                  
            ";
        }
    }  
}


function count_users()
{
    global $connection;

    $query = "SELECT * FROM users";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    return $user_count;
}



function count_users_by_role($role)
{
    global $connection;

    $query = "SELECT * FROM users WHERE user_role = '{$role}' ";
    $select_users_by_role = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_users_by_role);

    return $user_count;
}


function register_user()
{
    global $connection;
    
    $proceed = false;

    if (isset($_POST['registration_submit']))
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        $username = mysqli_real_escape_string($connection, $username);
        $email = mysqli_real_escape_string($connection, $email);
        $password = mysqli_real_escape_string($connection, $password);

        if (!empty($username) && !empty($email) && !empty($password))
        {
            $_SESSION['fm_message'] = 'Registration successfull';
            $proceed = true;
            unset($_SESSION['fm_element_id']);
        }
        else
        {
            $_SESSION['fm_message'] = 'Please<br>fill all the fields';
            $proceed = false;
            unset($_SESSION['fm_element_id']);
        }

        if ($proceed == true)
        {
            // $salt = retrieve_salt();

            // $password = crypt($password, $salt);
    
            $password = password_hash ($password, PASSWORD_BCRYPT, array ('cost' => 12) );

            $query =
            "
                INSERT INTO users
                    (
                        username, user_email, user_password, user_role
                    )
                VALUES
                    (
                        '{$username}', '{$email}', '{$password}', 'subscriber'
                    )
            ";
            
            $register_user_query = mysqli_query($connection, $query);
            
            if ($register_user_query)
            {
                unset($username);
                unset($email);
                unset($password);
            }
            else
            {
                die ("Query Failed " . mysqli_error($connection) . '' . mysqli_errno($connection));
            }
        }

        header("Location: registration.php");

    }
}


function users_online()
{
  
        global $connection;

        $session = session_id();
        $time = time();
        $time_out_in_seconds = 60;
        $time_out = $time - $time_out_in_seconds;

        $query = 
        "
            SELECT * FROM users_online
            WHERE session = '$session';
        ";

        $send_query = mysqli_query($connection, $query);
        
        $count = mysqli_num_rows($send_query);

        if ($count == NULL)
        {
            $query2 =
            "
                INSERT INTO users_online
                (
                    session, time
                )
                VALUES
                (
                    '$session', '$time'
                )
            ";
            mysqli_query($connection, $query2);
        }
        else
        {
            $query3 =
            "
                UPDATE users_online
                SET time = '$time'
                WHERE session = '$session'
            ";
            mysqli_query($connection, $query3); 
        }


        $query4 =
        "
            SELECT * FROM users_online
            WHERE time > '$time_out';
        ";

        $users_online_query = mysqli_query($connection, $query4);

        $count_user = mysqli_num_rows($users_online_query);

        
        return $count_user;

    } 


// users_online();


// COMMENTS

function select_all_comments()
{
    global $connection;

    $query = "SELECT * FROM comments";
    $select_all_comments_query = mysqli_query($connection, $query);
    $context = 'select all comments';
    if (control_query($select_all_comments_query, $context))
    {
        return $select_all_comments_query;
    }
}

function select_all_comments_by_post($post_id)
{
    global $connection;

    $query = 
    "
    SELECT * FROM comments 
    WHERE comment_post_id = " . mysqli_real_escape_string($connection, $post_id);
    
    $select_all_comments_by_post_query = mysqli_query($connection, $query);
    $context = 'select all comments by post';
    if (control_query($select_all_comments_by_post_query, $context))
    {
        return $select_all_comments_by_post_query;
    }
}


function select_all_approved_comments_by_post($post_id)
{
    global $connection;

    $query = 
    "
        SELECT * FROM comments 
        WHERE comment_post_id = $post_id 
        AND comment_status = 'approved' 
        ORDER BY comment_id DESC 
    ";
    

    $select_all_approved_comments_by_post_query = mysqli_query($connection, $query);
    $context = 'select all approved comments by post';
    if (control_query($select_all_approved_comments_by_post_query, $context))
    {
        return $select_all_approved_comments_by_post_query;
    }  
}


function show_comments_table($comments)
{
    if (!$comments->num_rows)
    {
        echo 
        "
        <h3 style= 'color: darkred'>
            There are no comments for this post
        </h3>
        ";
    }
    else
    {
        while($row = mysqli_fetch_assoc($comments))
        {
            $comment_id = $row['comment_id'];
            $comment_post_id = $row['comment_post_id'];

            $post = search_post_by_id($comment_post_id);

            if($post)
            {
                $post_address = '../post.php?p_id=' . $post['post_id'];
                $post_title = $post['post_title'];
                $post_id = $post['post_id'];
            }
            else
            {
                $post_address = '0';
                $post_title = 'no title';
            }
            

            $comment_author = $row['comment_author'];
            $comment_email = $row['comment_email'];
            $comment_content = $row['comment_content'];
            $comment_status = $row['comment_status'];
            $comment_date = date_create($row['comment_date']);
            $date_european_format = date_format($comment_date, 'd/m/y H:i:s');

            echo
            "
            <tr>
                <td>{$comment_id}</td>
                <td>{$comment_author}</td>
                <td>{$comment_content}</td>
                <td>{$comment_email}</td>
                <td>{$comment_status}</td>
                <td>
                    <a href='{$post_address}'>
                        {$post_title}
                    </a>
                </td>
                <td>{$date_european_format}</td>
 
                <td><a href='comments.php?comment_approve={$comment_id}&p_id={$post_id}'>Approve</a>
                    <br> 
                    <a href='comments.php?comment_reject={$comment_id}'>Reject</a>
                    <br> 
                    <a href='comments.php?comment_delete={$comment_id}'>Delete</a> 
                    
                </td>
            </tr>
            ";
        }
    }  
}





function show_post_comments_table($comments)
{
    if (!$comments->num_rows)
    {
        echo 
        "
        <h3 style= 'color: darkred'>
            There are no comments for this post
        </h3>
        ";
    }
    else
    {
        while($row = mysqli_fetch_assoc($comments))
        {
            $comment_id = $row['comment_id'];
            $comment_post_id = $row['comment_post_id'];

            $post = search_post_by_id($comment_post_id);

            if($post)
            {
                $post_address = '../post.php?p_id=' . $post['post_id'];
                $post_title = $post['post_title'];
                $post_id = $post['post_id'];
            }
            else
            {
                $post_address = '0';
                $post_title = 'no title';
            }
            

            $comment_author = $row['comment_author'];
            $comment_email = $row['comment_email'];
            $comment_content = $row['comment_content'];
            $comment_status = $row['comment_status'];
            $comment_date = date_create($row['comment_date']);
            $date_european_format = date_format($comment_date, 'd/m/y H:i:s');

            echo
            "
            <tr>
                <td>{$comment_id}</td>
                <td>{$comment_author}</td>
                <td>{$comment_content}</td>
                <td>{$comment_email}</td>
                <td>{$comment_status}</td>
                <td>
                    <a href='{$post_address}'>
                        {$post_title}
                    </a>
                </td>
                <td>{$date_european_format}</td>
 
                <td><a href='comments_post.php?comment_approve={$comment_id}&p_id={$post_id}'>Approve</a>
                    <br> 
                    <a href='comments_post.php?comment_reject={$comment_id}&p_id={$post_id}'>Reject</a>
                    <br> 
                    <a href='comments_post.php?comment_delete={$comment_id}&p_id={$post_id}'>Delete</a> 
                    
                </td>
            </tr>
            ";
        }
    }  
}



function comment_submit($post_id)
{
    global $connection;

    if (isset($_POST['comment_submit']))
        {    
            $post_id = $_GET['p_id'];
            $comment_author = $_POST['comment_author'];
            $comment_email = $_POST['comment_email'];
            $comment_content = $_POST['comment_content'];

            // fields validation
            if (!empty($comment_author) && !empty($comment_email) && !empty($comment_content))
            {
                $query = 
                    "
                        INSERT INTO comments
                        (
                            comment_post_id,
                            comment_author,
                            comment_email,
                            comment_content,
                            comment_status,
                            comment_date
                        )
                        VALUES
                        (
                            $post_id,
                            '{$comment_author}',
                            '{$comment_email}',
                            '{$comment_content}',
                            'pending',
                            now()
                        );
                    
                    ";

                $submit_comment_query = mysqli_query($connection, $query);

                $context = 'comment submit';
                if (control_query($submit_comment_query, $context))
                {
                    $_SESSION['fm_message'] = 'Comment Submitted';
                    unset($_SESSION['fm_element_id']);
                }

                // $query_count = 
                // "
                //     UPDATE posts
                //     SET post_comment_count = post_comment_count + 1
                //     WHERE post_id = $post_id;
                // ";

                // $comment_count_query = mysqli_query($connection, $query_count);

                // $context = 'update comment count';
                // control_query($comment_count_query, $context);


                // routine di reindirizzamento tramite JS
                // perché evidentemente header dava errore, qui
                echo
                "
                    <script>
                        window.location.href = 'post.php?p_id=$post_id';    
                    </script>
                ";
            }
            else
            {
                echo
                "
                    <script>
                        alert('Fields cannot be empty');
                    </script>
                ";
            }  
        }         
}


function delete_comment()
{

    global $connection;

    if (isset($_GET['comment_delete']))
    {
        $comment_delete_id = $_GET['comment_delete'];

        $query = "DELETE from comments WHERE comment_id = {$comment_delete_id} ";
        $delete_comment_query = mysqli_query($connection, $query); 
        $context = 'delete comment';
        control_query($delete_comment_query, $context);
        $_SESSION['message'] = 'Comment deleted';
        
        header("Location: comments.php");
    }
}



function delete_post_comment()
{

    global $connection;

    if (isset($_GET['comment_delete']))
    {
        $comment_delete_id = $_GET['comment_delete'];
        $post_id = $_GET['p_id'];

        $query = "DELETE from comments WHERE comment_id = {$comment_delete_id} ";
        $delete_comment_query = mysqli_query($connection, $query); 
        $context = 'delete comment';
        control_query($delete_comment_query, $context);
        $_SESSION['message'] = 'Comment deleted';
        
        header ("Location: comments_post.php?pc_id=" .$post_id ."");
    }
}




function approve_comment()
{
    global $connection;

    if (isset($_GET['comment_approve']))
    {
        $comment_approve_id = $_GET['comment_approve'];

        $query = 
        "
        UPDATE comments
        SET 
            comment_status = 'approved'
        WHERE comment_id = $comment_approve_id;
        ";

        $_SESSION['message'] = 'Comment approved';
        $approve_comment_query = mysqli_query($connection, $query); 
        $context = 'approve comment';
        
        control_query($approve_comment_query, $context);

        header("Location: comments.php");
    }
}



function approve_post_comment()
{
    global $connection;

    if (isset($_GET['comment_approve']))
    {
        $comment_approve_id = $_GET['comment_approve'];
        $post_id = $_GET['p_id'];

        $query = 
        "
        UPDATE comments
        SET 
            comment_status = 'approved'
        WHERE comment_id = $comment_approve_id;
        ";

        $_SESSION['message'] = 'Comment approved';
        $approve_comment_query = mysqli_query($connection, $query); 
        $context = 'approve comment';
        
        control_query($approve_comment_query, $context);

        header ("Location: comments_post.php?pc_id=" .$post_id ."");

    }
}



function reject_comment()
{

    global $connection;

    if (isset($_GET['comment_reject']))
    {
        $comment_reject_id = $_GET['comment_reject'];

        $query = 
        "
        UPDATE comments
        SET 
            comment_status = 'rejected'
        WHERE comment_id = $comment_reject_id;
        
        ";

        $reject_comment_query = mysqli_query($connection, $query); 

        $_SESSION['message'] = 'Comment rejected';
        $context = 'reject comment';
        control_query($reject_comment_query, $context);
        
        header('Location: comments.php');
    }
}




function reject_post_comment()
{

    global $connection;

    if (isset($_GET['comment_reject']))
    {
        $comment_reject_id = $_GET['comment_reject'];

        $post_id = $_GET['p_id'];

        $query = 
        "
        UPDATE comments
        SET 
            comment_status = 'rejected'
        WHERE comment_id = $comment_reject_id;
        
        ";

        $reject_comment_query = mysqli_query($connection, $query); 

        $_SESSION['message'] = 'Comment rejected';
        $context = 'reject comment';
        control_query($reject_comment_query, $context);
        
        header ("Location: comments_post.php?pc_id=" .$post_id ."");
    }
}




function count_comments_by_status($status)
{
    global $connection;

    $query = "SELECT * FROM comments where comment_status = '{$status}' ";
    $select_comments_by_status = mysqli_query($connection, $query);
    $comment_count = mysqli_num_rows($select_comments_by_status);

    return $comment_count;
}



function count_comments()
{
    global $connection;

    $query = "SELECT * FROM comments";
    $select_all_comments = mysqli_query($connection, $query);
    $comment_count = mysqli_num_rows($select_all_comments);

    return $comment_count;
}



function show_comments_under_post($comments)
{
    while ($row = mysqli_fetch_assoc($comments))
    {

        $comment_id = $row['comment_id'];
        
        $comment_date = date_create($row['comment_date']);
        $date_european_format = date_format($comment_date, 'd/m/y H:i:s');

        $comment_author = $row['comment_author'];

        $comment_content = $row['comment_content'];

        echo
            "
            <div class='media'>
                <a class='pull-left' href='#'>
                    <img class='media-object' src='https://picsum.photos/64/64?random={$comment_id}' alt=''>
                </a>
                <div class='media-body'>
                    <h4 class='media-heading'>
                        {$comment_author}

                        <small>{$date_european_format}</small>
                    </h4>
                    {$comment_content}
                </div>
            </div>

            ";
        

        

    }
}















?>