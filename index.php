<?php   
    include 'includes/header.php';
    include 'includes/functions.php';

    // set the number of posts per page
    $posts_per_page = 10;

    if (isset($_GET['auth']) && $_GET['auth'] == 'none')
    {
        echo 
        "
            <script>
                console.warn('those credentials do not belong to any registered user');
            </script>
        ";
    }
?>

<body>

    <!-- Navigation -->
    <?php
        include 'includes/navigation.php';
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <?php
                    $from = set_starting_post($posts_per_page);
                    
                    $posts = select_all_published_posts($from, $posts_per_page);
                    
                    show_posts($posts);
                ?>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php
                include 'includes/sidebar.php';
            ?>


        </div>
        <!-- /.row -->

        <hr>

        <!-- pagination -->
        <?php
            include 'includes/paginator.php';
        ?>

        <!-- Footer -->
        <?php
            include 'includes/footer.php';
        ?>


