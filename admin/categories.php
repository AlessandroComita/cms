<?php 
    include "includes/admin_header.php";
    include "../includes/functions.php";
?>

<body>

    <div id="wrapper">

        <?php
            select_all_posts();
        ?>

        <!-- Navigation -->
        <?php 
            include "includes/admin_navigation.php"
        ?>




        <?php
            if (isset($_SESSION['fm_message']))
            {
                include "includes/flash_message.php";
                unset($_SESSION['fm_message']);
                unset($_SESSION['fm_element_id']);
            }
        ?>





        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome, Admin
                            <small>Author</small>
                        </h1>
                        
                        <div class="col-xs-6">
                            <?php
                                add_category();
                            ?>

                            <!-- form aggiungi categoria -->
                            <form action="" method="POST">
                                <div class="form-group">
                                    <label for="cat_title">
                                        Add Category
                                    </label>
                                        <input 
                                        class="form-control"
                                        name="cat_title" type="text">
                                </div>

                                <div class="form-group">
                                        <input 
                                            class="btn btn-primary"
                                            name="cat_add_submit"
                                            value="Go"
                                            type="submit">
                                </div>
                            </form>


                            <!-- form modifica categoria -->
                            <?php
                                if (isset($_GET['update']))
                                {
                                    $cat_edit = get_category();

                                    echo
                                    "
                                    <form action='' method='POST' style='margin-top: 30px;'>
                                        <div class='form-group'>
                                        <label for='cat_title'>
                                         Edit Category
                                        </label>
                                        <input 
                                        value =  '{$cat_edit['cat_title']}'
                                        class='form-control'
                                        name='cat_title_edit' type='text'>
                                </div>

                                <div class='form-group'>
                                        <input 
                                            class='btn btn-primary'
                                            name='cat_edit_submit'
                                            value='Go'
                                            type='submit'>
                                </div>
                            </form>


                                    
                                    ";
                                }
                            ?>
                            

                        </div> <!-- forms -->
                        

                        <?php
                            $categories = select_all_categories();
                        ?>

                        <div class="col-xs-6">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category Title</th>
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        show_categories_table($categories);
                                    ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
