<?php 
    include "includes/admin_header.php";
    include "../includes/functions.php";
?>

<body>

    <div id="wrapper">

        <?php
            select_all_posts();
        ?>

        <!-- Navigation -->
        <?php 
            include "includes/admin_navigation.php"
        ?>




        <?php
            if (isset($_SESSION['fm_message']))
            {
                include "includes/flash_message.php";
                unset($_SESSION['fm_message']);
                unset($_SESSION['fm_element_id']);
            }
        ?>





        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome, 
                            <small>
                                <?php
                                                                                                            echo $_SESSION['user_firstname'];
                                                                                                            echo ' ';
                                                                                                            echo $_SESSION['user_lastname'];            

                                ?>
                            </small>
                        </h1>



                    

                        <?php
                            if (isset($_GET['source']))
                            {
                                $source = $_GET['source'];
                            }
                            else
                            {
                                $source = 0;
                            }

                            switch($source)
                            {
                                case 'add_post';
                                    include "includes/add_post.php";
                                    break;
                                case 'edit_post';
                                    include "includes/edit_post.php";
                                    break;
                                case '200';
                                    echo "Too much";
                                    break;
                                default:
                                    include "includes/view_all_posts.php";
                                    break;
                            }


                            
                        ?>






                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php
    include "includes/admin_footer.php";
?>
