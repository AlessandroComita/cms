<?php
    
    include "includes/admin_header.php";
    include "../includes/functions.php";

    if (isset($_SESSION['username']))
    {
       
        echo
        "
            <script>
                console.log('dentro la pagina PROFILE è presente la sessione e i dati dello username > ' + '{$_SESSION['username']}');
            </script>
        "
        ;

        $user = get_user($_SESSION['user_id']);

    }



?>

<body>

    <div id="wrapper">

        <?php
            select_all_posts();
        ?>

        <!-- Navigation -->
        <?php 
            include "includes/admin_navigation.php"
        ?>










        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            PROFILE Welcome, Admin
                            <small>Author</small>
                        </h1>


                        <h3>Edit User</h3>
<form action='' method='POST' enctype='multipart/form-data'>

    <div class="form-group">
        <label for="username">Username</label>
        <input 
            name='username'
            value="<?php echo $user['username'] ?>"
            class="form-control"  
            type="text">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input 
            name='user_email' 
            value="<?php echo $user['user_email'] ?>"
            class="form-control"
            type="email">
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input 
            name='user_password' 
            autocomplete = "off"
            class="form-control"  
            type="password">
    </div>
    
    <div class="form-group">
        <label for="firstname">Firstname</label>
        <input 
            name='user_firstname' 
            value="<?php echo $user['user_firstname'] ?>"
            class="form-control"  
            type="text">
    </div>

    <div class="form-group">
        <label for="lastname">Lastname</label>
        <input 
            name='user_lastname' 
            value="<?php echo $user['user_lastname'] ?>"
            class="form-control"  
            type="text">
    </div>

    
    <div class="form-group">
        <label for="image">Image</label>
        <img    
            src=
            "
                <?php 
                    $path = '../images/' . $user['user_image'];
                    echo $path;
                ?>
            "
            style="width: 10vw;"
        />
        
        <input 
            name='user_image' 
            class="form-control" 
            type="file"
            title=" "
            style="color: transparent;"
        />
    </div>


    <div class="form-group">
        <input 
            name="update_user"
            class="btn btn-primary"
            type="submit"
            value="Update Profile"
            >
    </div>
  
</form>

<?php
    update_user($user['user_id'], $user['user_image']);
?>
                    

                      






                    </div>
                </div>
                <!-- /.row -->




            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php
    include "includes/admin_footer.php";
?>