<?php 
    include "../includes/functions.php";
    include "includes/admin_header.php";
?>

<body>

    <div id="wrapper">

    <?php
        $count_user = users_online();
    ?>

    <?php
        select_all_posts();      
        $post_count = count_posts();
        $comment_count = count_comments();
        $user_count = count_users();
        $category_count = count_categories();
    ?>

        <!-- Navigation -->
        <?php 
            include "includes/admin_navigation.php"
        ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome, 
                            
                            <span style='font-weight: lighter; color: gray;'>
                                <?php echo $_SESSION['user_role']; ?>
                            </span>
                            
                            <span style='font-style: italic; color: darkred;'>
                                <?php echo $_SESSION['username']; ?>
                            </span>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->


               <!-- widget row -->
               <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-file-text fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class='huge'>
                                            <?php
                                                echo $post_count
                                            ?>
                                        </div>
                                            <div>Posts</div>
                                    </div>
                                </div>
                            </div>
                            <a href="posts.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                                      
                                    
                                        <div class="col-xs-9 text-right">
                                            <div class='huge'>
                                                <?php
                                                    echo $comment_count;
                                                ?>
                                            </div>
                                                <div>Comments</div>
                                        </div>
                                         
                                </div>
                            </div>
                            <a href="comments.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>


                                    <div class="col-xs-9 text-right">
                                            <div class='huge'>
                                                <?php
                                                    echo $user_count;
                                                ?>
                                            </div>
                                                <div>Users</div>
                                        </div>


                                </div>
                            </div>
                            <a href="users.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-list fa-5x"></i>
                                    </div>



                                    <div class="col-xs-9 text-right">
                                            <div class='huge'>
                                                <?php
                                                    echo $category_count;
                                                ?>
                                            </div>
                                                <div>Categories</div>
                                        </div>



                                </div>
                            </div>
                            <a href="categories.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            <?php
                // count of elements
                $draft_posts = count_posts_by_status('draft');
                $published_posts = count_posts_by_status('published');

                $approved_comments = count_comments_by_status('approved');
                $rejected_comments = count_comments_by_status('rejected');
                $pending_comments = count_comments_by_status('pending');

                $admin_users = count_users_by_role('admin');
                $subscriber_users = count_users_by_role('subscriber');
            ?>

                <div class="row">
                    <script type="text/javascript">
                    google.charts.load('current', {'packages':['bar']});
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                        ['Data', 'Count'],


                            <?php
                                $element_text = ['Posts', 'Published', 'Draft','Comments', 'Published', 'Rejected', 'Pending',  'Users', 'Admin', 'Subscribers', 'Categories'];
                                $element_count = [$post_count, $published_posts, $draft_posts, $comment_count, $approved_comments, $rejected_comments, $pending_comments, $user_count, $admin_users, $subscriber_users, $category_count ];
                                $col_num = count($element_count);

                                for ($i = 0; $i < $col_num; $i++)
                                {
                                    echo "['{$element_text[$i]}'" . ", " . "{$element_count[$i]}],";
                                }
                            ?>                    

                        ]);

                        var options = {
                        chart: {
                            title: '',
                            subtitle: '',
                        }
                        };

                        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));
                    }
                    </script>

                        <div id="columnchart_material" style="width: auto; height: 500px;"></div>
                        

                </div>



            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
<?php
    include "includes/admin_footer.php";
?>