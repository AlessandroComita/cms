<?php
    if (isset($_GET['source']) && $_GET['source'] == 'comments_by_post' && isset($_GET['p_id']))
    {
        $post_id = $_GET['p_id'];
        $comments = select_all_comments_by_post($post_id);

        $path = '?source=comments_by_post&p_id=' . $post_id;
        print_r($path);
        if (!$comments->num_rows)
        {
            $table_head = 'display: none;';
        }
    }

?>


<table 
    class="table table-bordered table-hover">
                        <thead
                            style="<?php echo $table_head ?>"
                        >
                            <tr>
                                <th>Id</th>
                                <th>Author</th>
                                <th>Comment</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>In Response to</th>
                                <th>Date</th>
                                <th>Operations</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php

                                    show_comments_table($comments);
                            ?>


                        </tbody>


                    </table>

