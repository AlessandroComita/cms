<?php
    $post = get_edit_post();
    update_post($post['post_id'], $post['post_image']);
?>


<h3>Edit Post</h3>
<form action='' method='POST' enctype='multipart/form-data'>

<div class="form-group">
    <label for="title">Post Title</label>
    <input name='title' class="form-control"  type="text" 
           value="<?php echo "{$post['post_title']}" ?>">      
</div>

<div class="form-group">
<label for="post_category_id">Category

</label>
    <select name="post_category_id" id="post_category_id">      
            <?php
                $categories = select_all_categories();
                while($row = mysqli_fetch_assoc($categories))
                {
                    $cat_id = $row['cat_id'];
                    $cat_title = $row['cat_title'];
                    echo 
                    "
                        <option value='{$cat_id}' 
                    ";
                    if ($cat_id == $post['post_category_id'])
                    {
                        echo " selected ";
                    }
                    echo
                    "
                        >{$cat_title}</option>
                    ";
                } 
            ?>
    </select>
    
</div>

<div class="form-group">
    <label for="author">Post Author</label>
    <input name='author' class="form-control" type="text"
        value="<?php echo "{$post['post_author']}" ?>">   
</div>

<div class="form-group">
    
    <!-- <input name="post_status" class="form-control" type="text" --> 

    
    <label for="status">Post Status</label>
    <select name="post_status">
                <option 
                    value='draft'
                    <?php 
                        if ($post['post_status'] == 'draft')
                        {
                            echo 'selected';
                        }
                    ?>
                >
                    Draft
                </option>
                <option 
                    value='published'
                    <?php 
                        if ($post['post_status'] == 'published')
                        {
                            echo 'selected';
                        }
                    ?>
                >
                    Published
                </option>
    </select>
    

</div>

<div class="form-group">
    <label for="image">Post Image</label><br>
        <img 
            style="max-width: 30vw;"
            src="../images/<?php echo $post['post_image'] ?>">
    <input 
        name="image"
        class="form-control" 
        type="file"
        title=" "
        style="color: transparent;"
    />

</div>

<br>

<div class="form-group">
    <label for="tags">Post Tags</label>
    <input name="tags" class="form-control" type="text"
    value="<?php echo "{$post['post_tags']}" ?>">
</div>

<div class="form-group">
    <label for="summernote">Post Content</label>
        <textarea 
            name="content" 
            class="form-control" 
            type="text"
            id="summernote" 
            cols="30" 
            rows="10">
            <?php echo "{$post['post_content']}" ?>
        </textarea>
</div>



<div class="form-group">
    <input 
        class="btn btn-primary"
        type="submit"
        name="submit_post_edit"
        value="Update Post"
        >
</div>

<?php
    // create_post();
?>      

</form>



