<h3>Users list</h3>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Picture</th>
            <th>Role</th>       
            <th>Operations</th>
        </tr>
    </thead>

    <tbody>

        <?php
            $users = select_all_users();
            show_users_table($users);
        ?>


    </tbody>


</table>

<?php


    alert_to_user();
    delete_user();
    make_admin_user();
    make_subscriber_user();
    

?>
