<table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Author</th>
                                <th>Comment</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>In Response to</th>
                                <th>Date</th>
                                <th>Operations</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php
                                $comments = select_all_comments();
                                show_comments_table($comments);
                            ?>


                        </tbody>


                    </table>

<?php


    alert_to_user();
    delete_comment();
    approve_comment();
    reject_comment();

?>
