<?php
    if ((isset($_GET['source']) && isset($_GET['u_id']) && $_GET['source']=='edit_user'))
    {
        $id_user_edit = $_GET['u_id'];
        $user = get_user($id_user_edit);
    }
    else
    {
        header ("Location: index.php");
    }

?>


<h3>Edit User</h3>
<form action='' method='POST' enctype='multipart/form-data'>

    <div class="form-group">
        <label for="username">Username</label>
        <input 
            name='username'
            value="<?php echo $user['username'] ?>"
            class="form-control"  
            type="text">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input 
            name='user_email' 
            value="<?php echo $user['user_email'] ?>"
            class="form-control"
            type="email">
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input 
            name='user_password' 
            autocomplete = "off"
            class="form-control"  
            type="password">
    </div>
    
    <div class="form-group">
        <label for="firstname">Firstname</label>
        <input 
            name='user_firstname' 
            value="<?php echo $user['user_firstname'] ?>"
            class="form-control"  
            type="text">
    </div>

    <div class="form-group">
        <label for="lastname">Lastname</label>
        <input 
            name='user_lastname' 
            value="<?php echo $user['user_lastname'] ?>"
            class="form-control"  
            type="text">
    </div>



    <div class='form-group'>
        <label for='user_role'>Role</label>
        <select name='user_role'>
            <option value='subscriber'>Select a role</option>
                <option 
                    value='admin'
                    <?php 
                        if($user['user_role'] == 'admin')
                        {
                            echo 'selected';
                        };
                    ?>
                    >Admin</option>
                <option value='moderator'
                    <?php 
                        if($user['user_role'] == 'moderator')
                        {
                            echo 'selected';
                        };
                    ?>
                >Moderator</option>
                <option value='author'
                    <?php 
                        if($user['user_role']=='author')
                        {
                            echo 'selected';
                        };
                    ?>
                >Author</option>
                <option value='subscriber'
                    <?php 
                        if($user['user_role']=='subscriber')
                        {
                            echo 'selected';
                        };
                    ?>
                >Subscriber</option>
        </select>
        
    </div>




    <div class="form-group">
        <label for="image">Image</label>
        <img    
            src=
            "
                <?php 
                    if (!$user['user_image'])
                    {
                        !$user['user_image'] = 'nopicture.jpg';
                    }
                    $path = '../images/' . $user['user_image'];
                    echo $path;
                ?>
            "
            style="width: 10vw;"
        />
        
        <input 
            name='user_image' 
            class="form-control" 
            type="file"
            title=" "
            style="color: transparent;"
        />
    </div>


    <div class="form-group">
        <input 
            name="update_user"
            class="btn btn-primary"
            type="submit"
            value="Update User"
            >
    </div>
  
</form>


<?php
    $current_image = $user['user_image'];
    update_user($id_user_edit, $current_image);
?>  