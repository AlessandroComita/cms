<h3>Add User</h3>
<form action='' method='POST' enctype='multipart/form-data'>

    <div class="form-group">
        <label for="username">Username</label>
        <input name='username' class="form-control"  type="text">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input name='user_email' class="form-control"  type="email">
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input name='user_password' class="form-control"  type="password">
    </div>
    
    <div class="form-group">
        <label for="firstname">Firstname</label>
        <input name='user_firstname' class="form-control"  type="text">
    </div>

    <div class="form-group">
        <label for="lastname">Lastname</label>
        <input name='user_lastname' class="form-control"  type="text">
    </div>



    <div class='form-group'>
        <label for='user_role'>Role</label>
        <select name='user_role'>
            <option value='subscriber'>Select a role</option>
                <option value='admin'>Admin</option>
                <option value='moderator'>Moderator</option>
                <option value='author'>Author</option>
                <option value='subscriber'>Subscriber</option>
        </select>
        
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        <input name='user_image' class="form-control" type="file">
    </div>


    <div class="form-group">
        <input 
            name="create_user"
            class="btn btn-primary"
            type="submit"
            value="Create User"
            >
    </div>
  
</form>


<?php
    create_user();
?>  