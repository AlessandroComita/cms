<?php

    if (isset($_POST['checkBoxArray']))
    {
        foreach ($_POST['checkBoxArray'] as $postValueId)
        {
            $bulk_options = $_POST['bulk_options'];

            switch($bulk_options)
            {
                case 'published':
                    update_post_status($bulk_options, $postValueId);
                    break;
                case 'draft':
                    update_post_status($bulk_options, $postValueId);
                    break;
                case 'delete':
                    delete_post_by_id($postValueId);
                    break;
                case 'clone':
                    clone_post($postValueId);
                    break;
                case 'reset':
                    reset_post_views($postValueId);
                    break;
            }            
        }
    }

?>




<form action="" method="POST">

    <table class="table table-bordered table-hover">

        <div 
            id="bulkOptionsContainer" 
            class="col-xs-4"
            style="margin-bottom: 30px"
        >
            <select 
                name="bulk_options" 
                class="form-control" 
                
                id="">
                <option value="">Post Operations</option>
                <option value="published">Publish</option>
                <option value="draft">Draft</option>
                <option value="clone">Clone</option>
                <option value="reset">Reset Views</option>
                <option value="delete">Delete</option>

            </select>

        </div>

        <div class="col-xs-4">
            <input
                type="submit"
                name="post-action-submit"
                class="btn btn-success"
                value="Apply"
            />
            <a 
                class="btn btn-primary"
                href="posts.php?source=add_post"
            >
                Add New
            </a>

        </div>

        <thead>
            <tr>
                <th>
                    <input id="selectAllBoxes" type="checkbox">
                </th>
                <th>Id</th>
                <th>Author</th>
                <th>Title</th>
                <th>Category</th>
                <th>Content</th>
                <th>Images</th>
                <th>Tags</th>
                <th>Status</th>
                <th>Comments</th>
                <th>Date</th>
                <th>Views</th>
                <th>Operations</th>
            </tr>
        </thead>

        <tbody>
            <?php
                $posts = select_all_posts();
                show_posts_table($posts);
            ?>
        </tbody>
    </table>

</form>

<?php
    delete_post();
    publish_post();

    if (isset($_GET['post_reset_views']))
    {
        $post_id_reset = $_GET['post_reset_views'];
        reset_post_views($post_id_reset);    
    }
?>
