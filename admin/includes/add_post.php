
<form action='' method='POST' enctype='multipart/form-data'>

    <div class="form-group">
        <label for="title">Post Title</label>
        <input name='title' class="form-control"  type="text">
    </div>
    

    <div class="form-group">
        <label for="post_category_id">Category</label>
            <select name="post_category_id" id="post_category_id">      
                    <?php
                        $categories = select_all_categories();
                        while($row = mysqli_fetch_assoc($categories))
                        {
                            $cat_id = $row['cat_id'];
                            $cat_title = $row['cat_title'];
                            echo 
                            "
                            <option value='{$cat_id}'>{$cat_title}</option>
                            ";
                        } 
                    ?>
            </select>
    </div>
    
    <div class="form-group">
        <label for="author">Post Author</label>
        <input name='author' class="form-control" type="text">
    </div>
    
    <div class="form-group">
        <select name="post_status">
            <option value='draft'>Post Status</option>
                <option value='draft'>Draft</option>
                 <option value='published'>Published</option>
        </select>
    </div>
   
    <div class="form-group">
        <label for="image">Post Image</label>
        <input name="image" class="form-control" type="file">
    </div>
    
    <div class="form-group">
        <label for="tags">Post Tags</label>
        <input name="tags" class="form-control" type="text">
    </div>
    
    <div class="form-group">
        <label for="summernote">Post Content</label>
        <textarea 
            name="content" 
            class="form-control" 
            type="text"
            id="summernote" 
            cols="30" 
            rows="10"></textarea>
    </div>
    
    <div class="form-group">
        <input 
            class="btn btn-primary"
            type="submit"
            name="create_post"
            value="Publish Post"
            >
    </div>

</form>



<?php
    create_post();
?>    


