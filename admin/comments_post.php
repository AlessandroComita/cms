<?php
    include "includes/admin_header.php";
    include "../includes/functions.php";
?>

<body>

    <div id="wrapper">

        <?php
            select_all_posts();
        ?>

        <!-- Navigation -->
        <?php 
            include "includes/admin_navigation.php"
        ?>


        <?php
            if (isset($_SESSION['fm_message']))
            {
                include "includes/flash_message.php";
                unset($_SESSION['fm_message']);
                unset($_SESSION['fm_element_id']);
            }
        ?>







        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Comments by post
                            <small>Author</small>
                        </h1>


                    
                        <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Author</th>
                                <th>Comment</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>In Response to</th>
                                <th>Date</th>
                                <th>Operations</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php

                                if (isset($_GET['pc_id']))
                                {
                                    $post_comments_id = $_GET['pc_id'];
                                    $comments = select_all_comments_by_post($post_comments_id);
                                    show_post_comments_table($comments);
                                }

                                
                            ?>


                        </tbody>


                    </table>

<?php


    alert_to_user();
    delete_post_comment();
    approve_post_comment();
    reject_post_comment();

?>




                    </div>
                </div>
                <!-- /.row -->




            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>



</body>

</html>
