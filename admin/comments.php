<?php
    include "includes/admin_header.php";
    include "../includes/functions.php";
?>

<body>

    <div id="wrapper">

        <?php
            select_all_posts();
        ?>

        <!-- Navigation -->
        <?php 
            include "includes/admin_navigation.php"
        ?>


        <?php
            if (isset($_SESSION['fm_message']))
            {
                include "includes/flash_message.php";
                unset($_SESSION['fm_message']);
                unset($_SESSION['fm_element_id']);
            }
        ?>







        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome, Admin
                            <small>Author</small>
                        </h1>


                    

                        <?php
                            if (isset($_GET['source']))
                            {
                                $source = $_GET['source'];
                            }
                            else
                            {
                                $source = 0;
                            }

                            switch($source)
                            {
                                case 'add_post';
                                    include "includes/add_post.php";
                                    break;
                                case 'edit_post';
                                    include "includes/edit_post.php";
                                    break;
                                case 'comments_by_post';
                                    include "includes/post_comments.php";
                                    break;
                                default:
                                    include "includes/view_all_comments.php";                               
                                    break;
                            }


                            
                        ?>






                    </div>
                </div>
                <!-- /.row -->




            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>



</body>

</html>
