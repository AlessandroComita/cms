<?php
    // session_start();
    include 'includes/header.php';
    include 'includes/functions.php';

?>

<body>

    <!-- Navigation -->
    <?php
        include 'includes/navigation.php';
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">


        <?php
            if (isset($_SESSION['fm_message']))
            {
                include "../cms/admin/includes/flash_message.php";
                unset($_SESSION['fm_message']);
                unset($_SESSION['fm_element_id']);
            }
        ?>


            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <?php
                    if (isset($_GET['author']))
                    {
                        $author = $_GET['author'];
                        $posts = select_posts_by_author($author);
                        show_posts($posts);
                    }
                ?>
      
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php

                include 'includes/sidebar.php';
            ?>

            

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <?php
            include 'includes/footer.php';
        ?>

