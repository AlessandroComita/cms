-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jul 19, 2022 at 01:21 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(3) NOT NULL,
  `cat_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Bootstrap5.1'),
(2, 'JavaScript ES 15'),
(3, 'PHP'),
(5, 'NoCode'),
(6, 'MySQL'),
(38, 'Laravel'),
(40, 'VueJS'),
(47, 'Uncategorized');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(3) NOT NULL,
  `comment_post_id` int(3) NOT NULL,
  `comment_author` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_status` varchar(255) NOT NULL,
  `comment_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `comment_post_id`, `comment_author`, `comment_email`, `comment_content`, `comment_status`, `comment_date`) VALUES
(2, 3, 'Alessandro Comità', 'alessandro-comita@email.it', 'Scrivo questo commento per provare la relativa funzionalità.', 'approved', '2022-06-01 08:21:21'),
(37, 22, 'Vel unde atque ratio', 'zipofu@mailinator.com', 'Ut fugit aut maxime', 'approved', '2022-07-05 01:50:16'),
(38, 22, 'Perspiciatis ea qui', 'gopyfy@mailinator.com', 'Voluptate rem exerci', 'approved', '2022-07-05 01:50:21'),
(39, 22, 'Reprehenderit rerum', 'heje@mailinator.com', 'Aut et saepe nostrud', 'approved', '2022-07-05 01:50:25'),
(40, 1, 'Coso', 'qucusasewy@mailinator.com', 'Dolores velit fugia', 'approved', '2022-07-05 02:34:49'),
(41, 1, 'Coso', 'qucusasewy@mailinator.com', 'Dolores velit fugia', 'approved', '2022-07-05 02:35:07'),
(42, 1, 'Voluptates quis repr', 'tyzuj@mailinator.com', 'Adipisicing tempore', 'pending', '2022-07-06 02:24:42'),
(43, 1, 'Consectetur dolore a', 'kokefovyx@mailinator.com', 'Et corporis esse lib', 'pending', '2022-07-06 02:25:41'),
(44, 23, 'In architecto at rep', 'vicavavah@mailinator.com', 'Molestias quia assum', 'pending', '2022-07-06 02:31:11'),
(45, 23, 'Incidunt provident', 'kagydi@mailinator.com', 'Laborum Officiis su', 'pending', '2022-07-06 02:31:52'),
(46, 25, 'Iste irure doloribus', 'zypeg@mailinator.com', 'Reprehenderit volup', 'pending', '2022-07-06 02:35:52'),
(47, 25, 'In labore molestias ', 'cujyt@mailinator.com', 'Et ullam sapiente qu', 'pending', '2022-07-06 02:36:08'),
(53, 3, 'Qui officiis eos cil', 'tylo@mailinator.com', 'Cumque et ut in quis', 'pending', '2022-07-10 15:49:38'),
(54, 1, 'Molestiae consequatu', 'qekaj@mailinator.com', 'Harum voluptatibus a', 'pending', '2022-07-10 15:49:54'),
(56, 49, 'Aut ad id magna face', 'qaqided@mailinator.com', 'In nihil aspernatur ', 'pending', '2022-07-14 02:53:13'),
(57, 49, 'Aut ad id magna face', 'qaqided@mailinator.com', 'In nihil aspernatur ', 'pending', '2022-07-14 02:56:17'),
(58, 49, 'Aut ad id magna face', 'qaqided@mailinator.com', 'In nihil aspernatur ', 'pending', '2022-07-14 02:57:24'),
(59, 49, 'Et aut minim et ut a', 'bopumet@mailinator.com', 'Temporibus ipsum la', 'pending', '2022-07-14 02:57:45'),
(60, 49, 'Et aut minim et ut a', 'bopumet@mailinator.com', 'Temporibus ipsum la', 'pending', '2022-07-14 02:57:52'),
(61, 49, 'Hic sit proident q', 'jyfehaje@mailinator.com', 'Fuga Et est quos ve', 'pending', '2022-07-14 02:58:22'),
(62, 49, 'Facere illo id aut p', 'xeviqon@mailinator.com', 'Id mollitia in volup', 'pending', '2022-07-14 03:01:13'),
(63, 49, 'Facere illo id aut p', 'xeviqon@mailinator.com', 'Id mollitia in volup', 'pending', '2022-07-14 03:01:37'),
(64, 49, 'Quia soluta eu minim', 'wavawe@mailinator.com', 'Libero sit dolore re', 'pending', '2022-07-14 03:01:46'),
(65, 49, 'Quia soluta eu minim', 'wavawe@mailinator.com', 'Libero sit dolore re', 'pending', '2022-07-14 03:04:31'),
(66, 49, 'Iste molestias qui s', 'wisafaxu@mailinator.com', 'Exercitation est eos', 'pending', '2022-07-14 03:04:37'),
(67, 49, 'Molestiae dolorem in', 'bypa@mailinator.com', 'Quis est rerum venia', 'pending', '2022-07-14 03:04:56'),
(68, 49, 'Dolores error odio a', 'jocunez@mailinator.com', 'Qui animi dolorum i', 'pending', '2022-07-14 03:05:03'),
(69, 49, 'Qui eaque molestiae ', 'zano@mailinator.com', 'Explicabo In proide', 'pending', '2022-07-14 03:06:49'),
(70, 49, 'Nostrud in soluta et', 'velimog@mailinator.com', 'Molestiae cillum eni', 'pending', '2022-07-14 03:11:42'),
(71, 49, 'Omnis nesciunt prae', 'qabafuxy@mailinator.com', 'Recusandae Sit dolo', 'pending', '2022-07-14 03:12:14'),
(72, 49, 'Tempore fugiat sapi', 'wivy@mailinator.com', 'Id in provident vo', 'pending', '2022-07-14 03:12:26'),
(73, 49, 'Error laborum sint a', 'vuqysyz@mailinator.com', 'Quisquam pariatur E', 'pending', '2022-07-14 03:13:11'),
(74, 49, 'Est sunt dolor sint', 'pizajozewa@mailinator.com', 'Qui labore veritatis', 'pending', '2022-07-14 03:13:26'),
(75, 49, 'Cupidatat dolores of', 'vidy@mailinator.com', 'Sunt cupidatat disti', 'pending', '2022-07-14 03:13:54'),
(76, 49, 'Facere omnis ut veli', 'gowufu@mailinator.com', 'Laudantium quam omn', 'pending', '2022-07-14 03:14:04'),
(78, 53, 'Ex error eum eos nul', 'menadar@mailinator.com', 'Veniam velit delect', 'pending', '2022-07-14 03:39:10'),
(83, 56, 'asddsa', 'calenadu@mailinator.com', '213132', 'pending', '2022-07-14 16:33:10'),
(84, 65, 'Repellendus Eaque q', 'jywedaxofo@mailinator.com', 'Aute et ea aut ea qu', 'pending', '2022-07-18 00:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(3) NOT NULL,
  `post_category_id` int(3) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_author` varchar(255) NOT NULL,
  `post_date` datetime NOT NULL,
  `post_image` text NOT NULL,
  `post_content` text NOT NULL,
  `post_tags` varchar(255) NOT NULL,
  `post_comment_count` int(11) DEFAULT 0,
  `post_status` varchar(255) NOT NULL DEFAULT 'draft',
  `post_views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_category_id`, `post_title`, `post_author`, `post_date`, `post_image`, `post_content`, `post_tags`, `post_comment_count`, `post_status`, `post_views`) VALUES
(3, 4, 'Javascript Course Post - Full tutorial', 'Belinda', '2022-07-04 00:32:09', 'coding-900x300-2.jpg', 'Wow, man this is really cool post? Can you call me?                    ', 'javascript2, course, class, belinda, coding, keyboard, layout', 5, 'published', 0),
(4, 3, 'How to install XAMPP on Windows systems', 'Mario Detzme', '2022-07-04 14:56:48', 'coding-900x300-3.jpg', 'XAMPP is a great tool. Since it gave me some hassles the first time I tried to install it to make it work, I created a tutorial to explain how to install it on Windows system without annoying issues.                ', 'XAMPP, PHP, databases, data, backend, coding', 4, 'published', 0),
(17, 5, 'Architecto ullam lau 444', 'Optio perspiciatis', '2022-07-04 00:09:45', '104828650-designers-are-working-on-the-desing-of-web-page-web-design-user-interface-ui-and-user-experience-ux- (1).webp', 'At elit voluptatem  444               ', 'Suscipit consequuntu', 4, 'published', 0),
(19, 5, 'Debitis tempore del', 'Vel veritatis vitae ', '2022-07-04 00:09:29', 'logo.jpg', 'Culpa sit saepe veli', 'Libero deserunt sunt', 4, 'published', 0),
(20, 5, 'Aut reprehenderit en', 'ultimo', '2022-07-04 00:42:22', 'favpng_logo-information-technology-high-tech.png', 'Sed culpa accusanti', 'Vitae qui molestiae ', 4, 'published', 0),
(27, 1, 'Pariatur Dolorem la', 'ultimo', '2022-07-18 02:35:01', '246.jpg', '                        asdsadas        ', 'Cumque quibusdam rep', 4, 'published', 0),
(45, 5, 'Nostrud possimus co', 'Dolores quas harum e', '2022-07-18 02:34:17', '074.jpg', '                                                adsdsacxc                        ', 'Sint laborum obcaeca', 4, 'published', 0),
(48, 5, 'Impedit pariatur P', 'Dolores quas harum e', '2022-07-14 00:55:10', 'hou-138-bloodwater-entity (1).jpg', '            dsadsaasddsa', 'Adipisicing cupidita', 4, 'published', 0),
(51, 1, 'ultimo', 'ultimo', '2022-07-14 01:42:56', 'm19-151-lightning-mare.jpg', '<p>ultimo</p>', 'ultimo', 0, 'published', 0),
(58, 1, 'ultimo', 'ultimo', '2022-07-18 02:34:27', '086.jpg', '                        <p>ultimo</p>                ', 'ultimo', 4, 'published', 0),
(59, 1, 'Pariatur Dolorem la', 'ultimo', '2022-07-18 02:34:06', '026.jpg', '                                    asdsadas                ', 'Cumque quibusdam rep', 4, 'published', 0),
(60, 5, '60', 'Vel veritatis vitae ', '2022-07-18 02:34:38', '128.jpg', '                                    Culpa sit saepe veli                        ', 'Libero deserunt sunt', 4, 'published', 0),
(61, 5, '61', 'Vel veritatis vitae ', '2022-07-18 02:34:48', '320.jpg', '                        Culpa sit saepe veli                ', 'Libero deserunt sunt', 4, 'published', 0),
(62, 5, '62', 'Dolores quas harum e', '2022-07-18 02:08:55', '5.jpg', '                                    adsdsacxc                ', 'Sint laborum obcaeca', 4, 'published', 0),
(65, 1, '65', 'ultimo', '2022-07-18 02:07:59', 'dd2.jpg', '                                    65        ', 'Cumque quibusdam rep', 4, 'published', 3),
(66, 6, 'Libero est quia mini', 'Est sed ratione earu', '2022-07-18 02:35:21', '443.jpg', '                        <p>ultimo</p>                ', 'Quas velit quia des', 4, 'published', 0),
(68, 47, 'Praesentium error di', 'Ut magnam sint quia', '2022-07-18 02:53:03', '005.jpg', '', 'Incidunt esse esse', 0, 'published', 0),
(69, 2, 'Sunt voluptatibus n', 'Aspernatur ullamco n', '2022-07-18 02:54:11', '013.jpg', '', 'Enim natus expedita ', 0, 'published', 0),
(70, 6, 'Odit reiciendis iust', 'Neque impedit magna', '2022-07-18 02:59:57', '122.jpg', '', 'Amet fugiat qui ips', 0, 'published', 0),
(71, 5, 'Dignissimos rerum ni', 'Aliquip quia quam at', '2022-07-18 03:00:42', '111_1.jpg', '', 'Blanditiis vero aute', 0, 'published', 0),
(72, 1, 'Dolore recusandae D', 'Eu omnis eveniet do', '2022-07-18 03:01:13', '129.jpg', 'dsadsa', 'Quam quia commodo co', 0, 'published', 0),
(73, 38, 'Commodo cillum disti', 'Omnis nihil sint min', '2022-07-18 03:02:27', '159.jpg', '                    ', 'Eiusmod architecto c', 4, 'published', 0),
(74, 2, 'Aut voluptatem qui i', 'Reprehenderit evenie', '2022-07-18 03:03:28', '154.jpg', '', 'Nisi quaerat alias m', 0, 'published', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_password` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_firstname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_lastname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_image` text CHARACTER SET utf8mb4 NOT NULL,
  `user_role` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `randSalt` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '$2y$10$iusesomecrazystrings22'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_password`, `user_firstname`, `user_lastname`, `user_email`, `user_image`, `user_role`, `randSalt`) VALUES
(129, 'manu', '$2y$12$XjJ/voBVPDJAzMaj/72Eiex55G14UVuvpc8xHwjovUGNXwHeUwhAa', 'Manu', 'Ele', 'allthefills@gmail.com', '035.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(130, 'ciccio', '$2y$12$RHgfzFWCenkYIq.RpIVpTO/j66M3OPrKFsCBjc4lqNKFhjHYmAFeW', 'Ciccio', 'Pasticcio', 'vadub2ar@mailinator.com', '053.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(131, 'dino', '$2y$12$IE48Glr0Sc9V40bF/2Sb5u5V4viGTVljX7Js.yCAiyk/DmW9frBim', 'Dino', 'Dinis', 'dddcccjedddgus@mailinator.com', '117.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(134, 'lollo', '$2y$10$iusesomecrazystrings2ulNMav2oucittP3uM8H4fxASbYy1uL9S', 'Lollo', 'Brigido', 'ggg@mailinator.com', 'grn-11-flight-of-equenauts.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(135, 'pippo', '$2y$10$iusesomecrazystrings2unL/KDrIXyAW9qOM2nxndkcSegZ4suBy', 'Pippo', 'Pippis', 'pippo@gmail.com', 'Pippo.webp', 'admin', '$2y$10$iusesomecrazystrings22'),
(136, 'dino', '$2y$10$iusesomecrazystrings2ui2H7NOplEbdWrhWw8ArormFbE96k6WW', 'Dino', 'Dinis', 'dinodini@ninetis.com', 'dinodini.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(138, 'pippo', '$2y$10$iusesomecrazystrings2u18R0xDd0hGwRPcah4aD5zQl6bsfffrO', '', '', 'pippo@pippo.com', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(139, 'pluto', '$2y$12$QXjtSF35ESi5umI7Srl5lOZ8fqYcgoQm4pwA//qAzgow5ZJSUVnRG', '', '', 'pluto@pluto.com', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(140, 'topo', '$2y$12$nZMBl4LkkW6AJ55eMq778uAzvW099SOA2VmJbTlc7Mq./Iif2.b6i', '', '', 'topo@topo.com', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(141, 'admin', '$2y$12$kldacAS8NYyCg04uuqn97.6wPIyauu00iPie4MYs9ox4FRh94X/KC', '', '', 'admin@admin.com', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(142, 'dado', '$2y$12$ZzNPlJ2KHq2UFrdcLuNBCOyLN0PC4hfJ07PcQDmUSocthqAZwf7p2', 'Dado', 'Box', 'dado@box.it', '441.jpg', 'admin', '$2y$10$iusesomecrazystrings22');

-- --------------------------------------------------------

--
-- Table structure for table `users_online`
--

CREATE TABLE `users_online` (
  `id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_online`
--

INSERT INTO `users_online` (`id`, `session`, `time`) VALUES
(1, '3qgq1ec1duub7can0vtkdgpifa', 1658161256),
(2, 'djphocuqflckfoj6srsp4b6gr8', 1658161148),
(3, 'copp6pic4oakndtpiaj996ikd7', 1658161220),
(4, '', 1658198010);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_online`
--
ALTER TABLE `users_online`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `users_online`
--
ALTER TABLE `users_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
