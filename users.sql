-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jul 15, 2022 at 11:20 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_password` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_firstname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_lastname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `user_image` text CHARACTER SET utf8mb4 NOT NULL,
  `user_role` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `randSalt` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '$2y$10$iusesomecrazystrings22'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_password`, `user_firstname`, `user_lastname`, `user_email`, `user_image`, `user_role`, `randSalt`) VALUES
(14, 'hhh', 'hhh', 'hhh', 'hhh', 'hhhh@mailinator.com', 'http-418-teapot-1408710394.png', 'admin', '$2y$10$iusesomecrazystrings22'),
(15, 'uuu', 'uuu', 'uuu', 'uuu', 'uuu@uuu.com', 'prm-86000-apex-devastator.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(112, 'dyzaqufabe', '$2y$10$iusesomecrazystrings2ujboSJJQUJPO/f1hYQtg/oS1e5ep8VOW', '', '', 'hifewo@mailinator.com', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(115, 'ducavup', '$2y$10$iusesomecrazystrings2ujboSJJQUJPO/f1hYQtg/oS1e5ep8VOW', '', '', 'pibi@mailinator.com', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(116, 'ccc', '$2y$10$iusesomecrazystrings2ufT/HwIIrW4ZuRq/k4imSLvAsiAv9dV2', '', '', 'ccc@ccc.it', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(117, 'ddd', '$2y$10$iusesomecrazystrings2uxxk.Cu3GSo2HRghxy5K/sukVIxsghp.', '', '', 'ddd@ddd.com', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(118, 'gehocem', '$2y$10$iusesomecrazystrings2ujboSJJQUJPO/f1hYQtg/oS1e5ep8VOW', '', '', 'pama@mailinator.com', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(119, 'demo', '$2y$10$iusesomecrazystrings2ui1qr860E30b0c9ijNqwCSwHnHdgz.1K', '', '', 'demo@123.ir', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(120, 'mina', '$2y$10$iusesomecrazystrings2ui1qr860E30b0c9ijNqwCSwHnHdgz.1K', 'Mina', 'Lollobrigida', 'allthefills@gmail.com', 'nopicture.jpg', 'admin', '$2y$10$iusesomecrazystrings22'),
(121, 'demo', '$2y$10$iusesomecrazystrings2ui1qr860E30b0c9ijNqwCSwHnHdgz.1K', '', '', 'demo@123.it', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(122, 'mina', '$2y$10$iusesomecrazystrings2u/AbuF7oPsGzErrKrjk/px2nY.aPMXzu', '', '', 'asd@asd.it', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(123, 'mina', '$2y$10$iusesomecrazystrings2uOtkWO6VKfAPyP.dxVPyhVRgEcogkYrK', '', '', 'ppp@ppp.it', '', 'admin', '$2y$10$iusesomecrazystrings22'),
(124, 'mina', '$2y$10$iusesomecrazystrings2uP762sce8Ex70pVDbuNqzlA8Nyz6XQ36', '', '', 'minamina@mina.com', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(125, 'yyy', '$2y$10$iusesomecrazystrings2u0x50RBssTuhH5TbPej95B.xxVfm599C', '', '', 'yyy@yyy.oy', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(126, 'ooo', '$2y$10$iusesomecrazystrings2uQq833TDk9pGzZyD9tKygJPxeyZwyPPa', '', '', 'ooo@ooo.op', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(127, 'vvv', '$2y$10$iusesomecrazystrings2uFqtk8z9ZMtpY6ZSUTjN.ljsESW/PvXO', '', '', 'vvv@vvv.vv', '', 'subscriber', '$2y$10$iusesomecrazystrings22'),
(128, 'ddd', '$2y$10$iusesomecrazystrings2uxxk.Cu3GSo2HRghxy5K/sukVIxsghp.', '', '', 'ddd@dd.dd', '', 'subscriber', '$2y$10$iusesomecrazystrings22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
