<?php
    // session_start();
    include 'includes/header.php';
    include 'includes/functions.php';

?>

<body>

    <!-- Navigation -->
    <?php
        include 'includes/navigation.php';
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">


        <?php
            if (isset($_SESSION['fm_message']))
            {
                include "../cms/admin/includes/flash_message.php";
                unset($_SESSION['fm_message']);
                unset($_SESSION['fm_element_id']);
            }
        ?>


            <!-- Blog Entries Column -->
            <div class="col-md-8">

            <?php
                if (isset($_GET['p_id']))
                {
                    $post_id = $_GET['p_id'];
                    $post = select_post($post_id);
                    show_post($post);
                }
                else
                {
                    header("Location: index.php"); 
                }

                comment_submit($_GET['p_id']);          

            ?>





                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="POST" 
                        action="post.php?p_id=<?php echo $post_id;?>">

                        <div class="form-group">
                            <label for="comment_author">Author</label>
                            <input name="comment_author" type="text" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="comment_email">Email</label>
                            <input name="comment_email" type="email" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label for="comment_content">Comment</label>
                            <textarea name="comment_content" class="form-control" rows="3" required></textarea>
                        </div>
                        <button 
                            name="comment_submit"
                            type="submit" 
                            class="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->


<?php
    $comments = select_all_approved_comments_by_post($post_id);
    show_comments_under_post($comments);
?>

               

                
                

                

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php

                include 'includes/sidebar.php';
            ?>

            

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <?php
            include 'includes/footer.php';
        ?>

