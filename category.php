<?php
    
    include 'includes/header.php';
    include 'includes/functions.php';

?>

<body>

    <!-- Navigation -->
    <?php
        include 'includes/navigation.php';
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

            <?php
                if ($_GET['category'])
                {
                    $category_id = $_GET['category'];
                    $posts = search_posts_by_id($category_id);
                    show_posts($posts);  
                }
                
            ?>


                

                

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php

                include 'includes/sidebar.php';
            ?>

            

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <?php
            include 'includes/footer.php';
        ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
